#define CPU "top -bn 1 | awk '/^%Cpu/ {printf \"^c#ff5555^  %.f%\", $2+$4+$6}'"

#define SOUND "som=$(amixer get Master | tail -n1 | sed -r 's/.*\\[(.*)%\\].*\\[(.*)\\]/[\\1%] [\\2]/')\n\
volume=$(echo $som | sed -r 's/\\[(.*)\\] \\[(.*)\\]/\\1/')\n\
status=$(echo $som | sed -r 's/\\[(.*)\\] \\[(.*)\\]/\\2/')\n\
if [ $status = on ]; then \n\
    echo \"^c#50fa7b^󰕾 ${volume}\"\n\
else\n\
    echo \"^c#50fa7b^󰝟 ${volume}\"\n\
fi"

#define MEM "echo \"^c#f1fa8c^󰍛 $(free -h | awk 'NR==2 { print $3\"/\"$2 }' | sed s/i//g)\""

#define TIME "echo \"^c#ffb86c^  $(date '+%b %d (%a) %H:%M%p')\""

#define CMDLENGTH 45
#define DELIMITER "  "
//#define CLICKABLE_BLOCKS

const Block blocks[] = {
        /*Command*/ /*Interval*/ /*Signal*/
	BLOCK(CPU,              2,        0),
	BLOCK(SOUND,            5,        10),
    BLOCK(MEM,              2,        0),
    BLOCK(TIME,             1,        0),
};
